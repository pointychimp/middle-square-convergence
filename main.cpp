#include <iostream>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "--------\nThis program takes your provided number and squares it.\n\
It then adds leading zeros if the square is not 12 digits long.\n\
Then it takes the middle 6 digits and repeats.\n\
It runs until zero is reached.\n--------\n";

    unsigned int number = 0;
    if (argc == 2)
    {
        number = atoi(argv[1]);
        cout << "Number is: " << number << endl;
    }
    else
    {
        cout << "Provide a starting number.\nSix digits would be a good \
idea, but not required. ";
        cin >> number;
    }

    unsigned int count = 0;
    for ( ; number != 0; count++)
    {
        number = number * number;
        number = (number % 1000000000) / 1000;
        cout << ".";

    }
    cout << "!\nInterations it took to reach zero: " << count << "\n--------" << endl;
}
